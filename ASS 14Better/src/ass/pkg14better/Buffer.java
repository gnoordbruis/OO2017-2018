/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.pkg14better;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Holubek S1006591 Gunnar Noordbruis S1008953
 */
public class Buffer <E>{
	
	private Lock lock = new ReentrantLock();
	private Condition full = lock.newCondition();
	private Condition empty = lock.newCondition();
	private E[] array;
	private int size;
	
	public Buffer (Class <E> c, int size){
		array = (E[]) Array.newInstance(c,size);
		this.size=size;
	}
	
	public void put (E element){
		lock.lock();
		try{
			while(full()){
				full.await();
			}
			array[size-1]=element;
			empty.signalAll();
		}
		catch (InterruptedException ex) {
			Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
		}				finally{
			lock.unlock();
		}
	}
	public E get() throws InterruptedException {
		lock.lock();
		try{
			while(empty()){
				empty.await();
			}
			E temp=null;
			
			for(int i=0;i<size;i++){
				if(array[i]!=null){
					temp = array[i];
					array[i]=null;
					full.signalAll();
					return temp;
				}
			}
			return temp;
		}
		finally{
			lock.unlock();
		}
	}
	
	public int length(){
		return array.length;
	}
	
	public boolean full(){
		boolean tempFull=true;
		for(int i=0;i<size;i++){
				if(array[i]==null){
					tempFull=false;
					for(int x=i; x<size-1;x++){
						array[x]=array[x+1];
					}
					array[size-1]=null;
				}
			}
		return tempFull;
	}
	
	public boolean empty(){
		boolean tempEmpty=true;
		
		for(int i=0;i<size;i++){
			if(array[i]!=null)
				tempEmpty=false;
		}
		return tempEmpty;
	}
}
