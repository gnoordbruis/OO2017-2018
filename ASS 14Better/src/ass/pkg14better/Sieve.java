/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.pkg14better;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruben Holubek S1006591 Gunnar Noordbruis S1008953
 */
public class Sieve implements Runnable{
	private final Buffer predecessor;
	private Buffer successor = null;
	private final int prime;
	private int ranking;
	private Sieve sucSieve = null; 
	
	public Sieve (Buffer predecessor, int prime, int ranking){
		this.predecessor = predecessor;
		this.prime = prime;
		this.ranking = ranking;
	}

	@Override
	public void run() {
		while(!predecessor.empty()){
			try {	
				int primeToTest = (int) predecessor.get();
				
				if(primeToTest%prime!=0){
					if(successor==null){
						System.out.println(ranking + ": " + primeToTest);
						successor = new Buffer(Integer.class, 10);
						int n = ranking+1;
						sucSieve = new Sieve (successor,  primeToTest, n);
					}
					else{
					successor.put(primeToTest);
					Thread T2 = new Thread(sucSieve);
					T2.start();
					}
				}
			}
			catch (InterruptedException ex) {
				Logger.getLogger(Sieve.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
}
