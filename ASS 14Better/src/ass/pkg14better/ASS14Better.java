/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass.pkg14better;

import java.util.Random;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ruben Holubek S1006591 Gunnar Noordbruis S1008953
 */
public class ASS14Better {

	/**
	 * @param args the command line arguments
	 */

	
	public static void main(String[] args) throws InterruptedException {
		int limit = 1010;
		Buffer predecessor = new Buffer(Integer.class, 10);
		predecessor.put(2);
		int x=3;
		for(;x<20;x+=2){
			predecessor.put(x);
		}
		Buffer successor = new Buffer(Integer.class, 10);
		Sieve first = new Sieve(predecessor, 2, 2);
		Thread T1 = new Thread(first);
		T1.start();
		System.out.println("1: 2");
		for(;x<limit+2;x+=2){
			predecessor.put(x);
		}
	}
	
}
